// Fill out your copyright notice in the Description page of Project Settings.


#include "TopDownShooter_IGameActor.h"

// Add default functionality here for any ITopDownShooter_IGameActor functions that are not pure virtual.

EPhysicalSurface ITopDownShooter_IGameActor::GetSurfaceType()
{
	return EPhysicalSurface::SurfaceType_Default;
}

TArray<UTopDownShooter_StateEffect*> ITopDownShooter_IGameActor::GetAllCurrentEffects()
{
	TArray<UTopDownShooter_StateEffect*> Effect;
	return Effect;
}

void ITopDownShooter_IGameActor::RemoveEffect(UTopDownShooter_StateEffect* RemoveEffect)
{
}

void ITopDownShooter_IGameActor::AddEffect(UTopDownShooter_StateEffect* newEffect)
{
}
